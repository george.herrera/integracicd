﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class HoraWebForm : System.Web.UI.Page
{
    protected void ButtonHora_Click(object sender, EventArgs e)
    {
        LabelHora.Text = "La hora actual es " + DateTime.Now;
    }
}
