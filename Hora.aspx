﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Hora.aspx.cs" Inherits="HoraWebForm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Hora.aspx</title>
</head>
<body>
    <form id="formHora" runat="server">
    <div>
        <asp:Button ID="ButtonHora" runat="server" Text="Pulse el botón para consultar la hora" OnClick="ButtonHora_Click" />
        <br />
        <br />
        <asp:Label ID="LabelHora" runat="server"></asp:Label></div>
    </form>
</body>
</html>
